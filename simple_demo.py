# coding: utf-8
from haruzirasdk import hz_client_tcp_communication as hzcomm

clt_tcp_comm = hzcomm.ClientTcpCommunication()

clt_tcp_comm.ServerPortNo = 46000               #port number of the destination
clt_tcp_comm.ServerIP = "192.168.1.6"           #ip address of the destination

clt_tcp_comm.ReqSendDataText = "Hello Haruzira!"
timestamp = clt_tcp_comm.sendSpeechDataEx()

if(timestamp is not None):
    print("success. timestamp[{}]".format(timestamp))
else:
    print("failed to send.")

#Stop listener thread.
clt_tcp_comm.cancelAsynchronousListener()