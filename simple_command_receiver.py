# coding: utf-8
import time
import threading
from haruzirasdk import hz_client_tcp_communication as hzcomm
 
#variable for terminal event.
terminate_flg = False

#create instance
clt_tcp_comm = hzcomm.ClientTcpCommunication()
 
#Callback funtion
def rcv_send_speech_recognition_command(cmd_info):
    print("Ip[{0:s}], Port[{1:d}], Command[{2:s}], Timestamp[{3:s}]".format(cmd_info.ip_addr, cmd_info.port, cmd_info.command, cmd_info.timestamp))

    global terminate_flg
    #check received command
    if(cmd_info.command.lower() == "weather information"):
        msg = "It's sunny today.\r\nBut, It will probably be rainy tomorrow."
    else:
        msg = "Invalid command name. [{0:s}]\r\nI can't work your command.".format(cmd_info.command)
    
    #send speech data
    clt_tcp_comm.ServerIP = cmd_info.ip_addr
    clt_tcp_comm.ServerPortNo = cmd_info.port
    clt_tcp_comm.ReqSendDataText = msg
    clt_tcp_comm.sendSpeechDataEx()
    #set terminate flag.
    terminate_flg = True
 
 
#regist callback function.
clt_tcp_comm.evNotifySendSpeechRecognitionCommand = rcv_send_speech_recognition_command
 
#set listener port number and start listener.
clt_tcp_comm.ReceivePort = 46300
#set as unnecessary message the 'speech completion notice'.
clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = hzcomm.HzSpCompletionNoticeNecessity.NoNeed.value
#start listener for asynchronous message.
clt_tcp_comm.startAsynchronousListener()
 
#waiting for until a command is received. 
while True:
    if(terminate_flg):
        print("terminate this process.")
        break
    time.sleep(1)

#Need to stop listener thread when before your program complete.
clt_tcp_comm.cancelAsynchronousListener()
