# coding: utf-8
import time, threading, datetime
import sys, signal, re
from haruzirasdk import hz_client_tcp_communication as hzcomm

#region VoiceClock is a class for sending current time and receiving voice commands.
class VoiceClock(object):

    # <summary>
    # Construct
    # </summary>
    # <param name="ip">ip address of the distination</param>
    # <param name="port">port number of the distination</param>
    def __init__(self, ip, port):
        self.__alarm_interval = 10
        self.__flg_stop = False
        self.__flg_term = False
        self.__th_worker = None
        self.__stop_evt = threading.Event()

        #for sending current time messages.
        self.__clt_tcp_comm = hzcomm.ClientTcpCommunication()
        self.__clt_tcp_comm.ServerIP = ip
        self.__clt_tcp_comm.ServerPortNo = port
        self.__clt_tcp_comm.ReceivePort = 46210
        self.__clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = hzcomm.HzSpCompletionNoticeNecessity.NoNeed.value
        self.__clt_tcp_comm.ReqSendDataSpeechRepeat = 1
        self.__clt_tcp_comm.startAsynchronousListener()
        self.__clt_tcp_comm.setTraceOutPut(False)

        #for receiving voice commands.
        self.__cmd_rcv_com = hzcomm.ClientTcpCommunication()
        self.__cmd_rcv_com.ReceivePort = 46200
        self.__cmd_rcv_com.ReqSendDataCompletionNoticeNecessity = hzcomm.HzSpCompletionNoticeNecessity.NoNeed.value
        self.__cmd_rcv_com.startAsynchronousListener()
        self.__cmd_rcv_com.setTraceOutPut(False)

        #regist callback functions
        self.__cmd_rcv_com.evNotifySendSpeechRecognitionCommand = self.notify_send_speech_recognition_command
        self.__clt_tcp_comm.evNotifyMessageEvent =  self.notify_communicaton_message
        self.__clt_tcp_comm.evNotifyReceivedDisconnectEvent = self.notify_received_disconnect

    #region define accessor
    def get_flg_term(self):
        return self.__flg_term

    def set_flg_term(self, value):
        self.__flg_term = value

    def get_th_worker(self):
        return self.__th_worker

    def set_th_worker(self, value):
        self.__th_worker = value

    flg_term = property(get_flg_term, set_flg_term)
    th_worker = property(get_th_worker, set_th_worker)
    #endregion

    #region call back functions
    ###############################################################
    # call back functions for haruzirasdk.
    # called when occurred events on a network communication.
    ###############################################################
    # notify when 'Send Speech Recognition Command' message received.
    # <param name="cmd_info">command infomation</param>
    def notify_send_speech_recognition_command(self, cmd_info):
        print("Received, Send Speech Recognition Command;\n  Ip[{0:s}], Port[{1:d}], Mode[{2:d}], Command[{3:s}], Timestamp[{4:s}]".format(cmd_info.ip_addr, cmd_info.port, cmd_info.mode, cmd_info.command, cmd_info.timestamp))

        announce = ""
        #check received command
        if (cmd_info.command.lower() == "start voice clock"):
            self.__flg_stop = False
            announce = "音声時計を開始します。"
            if(self.__stop_evt.is_set() != True):
                self.__stop_evt.set()
        elif (cmd_info.command.lower() == "stop voice clock"):
            self.__flg_stop = True
            announce = "音声時計を停止しました。"
        elif (cmd_info.command.lower() == "terminate voice clock process"):
            self.__flg_term = True
            announce = "音声時計プロセスを終了しました。"
            if(self.__stop_evt.is_set() != True):
                self.__stop_evt.set()
        else:
            if (re.match(r"^(?=.*インターバル)(?=.*[\d]+)(?=.*(変更|設定))", cmd_info.command)):
                vals = re.match(r".*?(\d+).*", cmd_info.command)
                interval = int(vals.group(1))
                #if (interval >= 1 and interval <= 60):
                if (1 <= interval <= 60):
                    self.__alarm_interval = interval
                    announce = "送信インターバルを{0:d}分に設定しました。".format(self.__alarm_interval)
                else:
                    announce = "送信インターバルは、１分～６０分の範囲で指定して下さい。[指定された値：{0:d}分]".format(interval)
            elif (re.match(r"^(?=.*(男性|女性))(?=.*(変更|設定))", cmd_info.command)):
                vals = re.match(r".*?(男性|女性).*", cmd_info.command)
                if(vals.group(1) == "女性"):
                    self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Female.value
                    self.__cmd_rcv_com.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Female.value
                else:
                    self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Male.value
                    self.__cmd_rcv_com.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Male.value
                announce = "{0:s}に設定しました。".format(vals.group(1))
            else:
                announce = "該当するコマンドが有りませんでした。"

        print("announce [{0:s}]".format(announce))

        try:
            #send speech data.
            self.send_responce(cmd_info, announce)
        except Exception as ex:
            print(ex)

    # notify when occurred issue that disconnected from access point or on receiving threads.
    # <param name="msg">message</param>
    # <param name="st">status</param>
    def notify_received_disconnect(self, msg, st):
        print("{0:s}, Status[0x{1:02x}]".format(msg, st))
        #self.__flg_stop = True

    # notify when occurred issue for some reasons(except disconnect).
    # <param name="msg">message</param>
    # <param name="msg_id">message id</param>
    # <param name="err_code">error code</param>
    def notify_communicaton_message(self, msg, msg_id, err_code):
        print("Message ID[0x{0:02x}], Error Code[0x{1:02x}], {2:s}".format(msg_id, err_code, msg))
        #self.__flg_stop = True

    #endregion define call back function 

    #cancel listeners for receiving asynchronous message.
    def cancel_asynchronous_listener(self):
        self.__cmd_rcv_com.cancelAsynchronousListener()
        self.__clt_tcp_comm.cancelAsynchronousListener()

    #send a message of the current time.
    def send_current_time(self, msg):
        self.__clt_tcp_comm.ReqSendDataText = msg
        return self.__clt_tcp_comm.sendSpeechDataEx()

    #send a responce message when recieved 'Send Speech Recognition Command'.
    def send_responce(self, cmd_info, msg):
        self.__cmd_rcv_com.ServerPortNo = cmd_info.port
        self.__cmd_rcv_com.ServerIP = cmd_info.ip_addr
        self.__cmd_rcv_com.ReqSendDataText = msg
        self.__cmd_rcv_com.sendSpeechDataEx()

    # thread worker to send voice clock messsage.
    def worker_clock(self):
        self.__stop_evt.wait()
        print("start worker thread.")
        while True:
            try:
                if(self.__flg_term):
                    break
                self.__stop_evt.clear()
                nowtime = datetime.datetime.now().time()

                if(self.__flg_stop != True and nowtime.second == 0 and (nowtime.minute % self.__alarm_interval == 0)):
                    print("alarm time.[{0}]".format(nowtime))
                    msg = "{0:02d}:{1:02d}です。".format(nowtime.hour, nowtime.minute)
                    time_stamp = self.send_current_time(msg)
                    if(time_stamp is not None):
                        print("success. timestamp[{0:s}]".format(time_stamp))
                    else:
                        print("failed to send.")
            
                time.sleep(1)

                if(self.__flg_stop):
                    print("stop thread.")
                    self.__stop_evt.wait()
                    self.__flg_stop = False
                    print("restart thread.")
            
            except Exception as ex:
                print("exception: {0:s}".format(ex))
                break

#endregion

#========== main routine ===================
#create instance. (Haruzira's ip and port)
voice_clock = VoiceClock("192.168.1.7", 46000)

#signal handler
def exit_signal_recieved(signum, frame):
    global voice_clock
    if(signum == signal.SIGINT):
        print("received interrupt signal. (ctrl+c)")
    elif(signum == signal.SIGTERM):
        print("received termination signal. (kill -15)")
    voice_clock.flg_term = True
    time.sleep(2)
    #close receiving listener.
    voice_clock.cancel_asynchronous_listener()
    exit()

for sig in (signal.SIGINT, signal.SIGTERM):
    signal.signal(sig, exit_signal_recieved)



#start working thread.
voice_clock.th_worker = threading.Thread(target=voice_clock.worker_clock)
voice_clock.th_worker.start()

#waiting for working thread until it complete.
voice_clock.th_worker.join()

#close receiving listener.
voice_clock.cancel_asynchronous_listener()
