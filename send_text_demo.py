# coding: utf-8
import threading
import sys
import time
import re
import feedparser
import hz_sample_demo


###############################################################
# en:global variable.
# ja:グローバル変数
###############################################################
sample_demo = None   #for HzSampleDemo class instance



###############################################################
# en:Language message tables for stdout.
# ja:標準出力用言語テーブル
###############################################################
strings_en = {"input_data":"Input  speech text：", "irregular_num":"Inpted a irregular number\nInput a number again.\n", \
              "end_rss":"The thread of sending a rss data was aborted.", "stop_listener":"The listener was stopped.\n", \
              "cancel_send":"Sending a speech data was canceld.\n", "rec_comp":"Received, Speech Completion Notice.", \
              "rcv_command":"Received, Send Speech Recognition Command.\n", "rss_url":"http://www.voanews.com/api/zq$omekvi_", \
              "invalid_command":"Invalid command data received. The command '{}' can not any work.", "weather_msg":"The weather status at current time.\nTemperature:[2.0 degrees]\nHumidity:[45 percent]\nAir pressure:[598 hectopascal]", \
              "weather_command":"weather information", "my_locate":"I am on Mount Shasta.\n",  \
              "completion_notice":"If you want to need the completion notice, enter 'y'. or else 'n'.\n" \
             }
strings_ja = {"input_data":"読み上げ文字列を入力して下さい。：", "irregular_num":"不正な番号が入力されました。\n再入力して下さい。\n", \
              "end_rss":"RSSニュースデータ送信スレッド終了", "stop_listener":"Listenerを終了しました。\n", \
              "cancel_send":"読み上げデータ送信を中断しました。\n", "rec_comp":"読み上げ完了通知を受信しました。", \
              "rcv_command":"音声認識コマンド送信メッセージを受信しました。", "rss_url":"http://www3.nhk.or.jp/rss/news/cat0.xml", \
              "invalid_command":"不明なコマンドを受信しました。コマンド「{}」に対する処理は実行できません。", "weather_msg":"現在の気象情報をお知らせします。\n気温「１０℃」\n湿度「５０％」\n気圧「980ヘクトパスカル」です。", \
              "weather_command":"気象状況", "my_locate":"こちらは、高尾山です。\n", \
              "completion_notice":"読み上げ完了通知の送信を期待する場合は'y', そうでなければ'n'を入力して下さい。\n" \
             }

#menu of english
menu_en = '''
=============================================================================
    Menu for demo
=============================================================================
[0]:send a speech data          [1]:send a rss news data(5 minutes interval)
[2]:stop a listener             [3]:cancel sending a speech data
[4]:set and get, time out value [5]:SpeechCompletionNotice Necessity
[99]:quit(exit)
=============================================================================

Input a number：
'''

#menu of japanese
menu_ja = '''
=============================================================================
    デモ用メニュー
=============================================================================
[0]:読み上げデータ送信          [1]:RSSニュースデータ送信
[2]:Listener停止                [3]:読み上げデータ送信の停止（中断）
[4]:応答受信時タイムアウト値の取得・設定
[5]:読み上げ完了通知の要否
[99]:終了(QUIT)
=============================================================================

[番号]を入力して下さい：
'''

# en:get a parameter to set language code
# ja:言語設定を行うためのパラメータ取得
strings = strings_en
argvs = sys.argv
lang = ""
if(len(argvs) > 1):
    lang = argvs[1].lower()
    if(lang == "ja"):
        menu = menu_ja
        strings = strings_ja
    else:
        menu = menu_en
else:
    menu = menu_en



###############################################################
# en:call back functions when occurred issues on connecting
# ja:通信イベントコールバック関数
###############################################################
# <summary>
# en:notify when occurred issue that disconnected from access point or on receiving threads.
# ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
# </summary>
# <param name="msg">message</param>
# <param name="st">status</param>
def notify_received_disconnect(msg, st):
    print("{0:s}, Status[0x{1:02x}]".format(msg, st))

# <summary>
# en:notify when occurred issue for some reasons(except disconnect).
# ja:通信障害（切断以外）発生時通知イベント
# </summary>
# <param name="msg">message</param>
# <param name="msg_id">message id</param>
# <param name="err_code">error code</param>
def notify_communicaton_message(msg, msg_id, err_code):
    print("Message ID[0x{0:02x}], Error Code[0x{1:02x}], {2:s}".format(msg_id, err_code, msg))

# <summary>
# en:notify when received a complition notice that made a speech.
# ja:読み上げ完了通知受信時通知イベント
# </summary>
# <param name="result">submit result</param>
# <param name="time_stamp">time stamp</param>
def rcv_notify_complete_speech(result, time_stamp):
    print("{0:s}：result[0x{1:02x}], time stamp[{2:s}]".format(strings["rec_comp"], result, time_stamp))

# <summary>
# en:notify when received a complition notice that made a speech.
# ja:音声認識コマンド送信メッセージ受信時イベント
# </summary>
# <param name="cmdInfo">HzSpeechRecognitionCommandInfo class object</param>
def rcv_send_speech_recognition_command(cmdInfo):
    print("{0:s} : Ip[{1:s}], Port[{2:d}], Mode[{3:d}], Command[{4:s}], Timestamp[{5:s}]".format(strings["rcv_command"], cmdInfo.ip_addr, cmdInfo.port, cmdInfo.mode, cmdInfo.command, cmdInfo.timestamp))
    if(cmdInfo.command.lower() == strings["weather_command"]):
        msg = strings["weather_msg"]
    else:
        msg = strings["invalid_command"].format(cmdInfo.command)
    
    sample_demo.server_port = cmdInfo.port
    sample_demo.server_ip = cmdInfo.ip_addr
    sample_demo.send_speech_data(strings["my_locate"] + msg)



###############################################################
# en:create instance of demo class（regist callback functions）
# ja:デモクラスのインスタンス生成（コールバック関数の登録）
###############################################################
sample_demo = hz_sample_demo.HzSampleDemo(rcv_notify_complete_speech, notify_communicaton_message, notify_received_disconnect, rcv_send_speech_recognition_command)

###############################################################
#sample_demo = HzSampleDemo.new()
# en:set some informations about access point.
# ja:接続先情報の設定
###############################################################
sample_demo.server_port = 46000
sample_demo.server_ip = "192.168.1.2"

###############################################################
# en:set listener port to receive messages of asynchronous.
# ja:非同期メッセージ受信Listener portの設定
###############################################################
sample_demo.receive_port = 46100

###############################################################
# en: you can make a set if you need authentication by account and encryption.
# ja:アカウント認証と暗号化を行いたい場合は、下記情報を設定する。
###############################################################
#account = "Raspberry Pi"
#passwd = "test123Passwd"
#enc_key = "12345user1234"
#account = None
#passwd = None
#enc_key = None

###############################################################
# en:If you want to receive voice command, need to Start listener.
#    Also if you want to decrypt that be encrypted voice command, 
#    need to set 'ReceivedDataDecryptionKey' property.
# ja:音声認識コマンドを受信する場合は、Listenerを起動する。
#    また、暗号化されたコマンドを受信する場合は、ReceivedDataDecryptionKey
#    に複合化キーをセットする。
###############################################################
sample_demo.rcv_data_decryption_key = "test23key"
sample_demo.start_asynchronous_listener()

###############################################################
# en:thread for rss
# ja:RSS処理スレッド
###############################################################
stop_event_rss = threading.Event()
def rss_worker():
    rss_url = strings["rss_url"]
    stop_flg = False
    stop_event_rss.clear()
    while True:
        try:
            rss = feedparser.parse(rss_url)
            rss_data = ""
            for entry in rss.entries:
                if(lang == "ja"):
                    rss_data = rss_data + re.sub(r"[ ]+", '、', entry.title) + "\n"
                else:
                    rss_data = rss_data + entry.title + "\n"
            print("\n\n" + rss_data + "\n\n")
            sample_demo.send_speech_data(rss_data)
            #sample_demo.send_speech_data(rss_data, account, passwd, enc_key)
            #interval time is every 15 minutes.
            for tm in range(450):
                time.sleep(2)
                if(stop_event_rss.is_set()):
                    stop_flg = True
                    break

            if(stop_flg):
                break
        except Exception as ex:
            print(ex.args)
            break
    print("Complete rss thread.")


###############################################################
# en:start main proccesing
# ja:メイン処理開始
###############################################################
th_rss = None    #thread object for rss
print(menu)
while True:
    s = input().strip()
    try:
        num = -1 if s == "" else int(s)
    except Exception as ex:
        num = -2

    if(num == 0): 
        print(strings["input_data"])
        msg = input().strip()
        print(msg)
        sample_demo.send_speech_data(msg)
        #sample_demo.send_speech_data(msg, account, passwd, enc_key)
    elif(num == 1):
        try:
            th_rss = threading.Thread(target=rss_worker)
            th_rss.start()
        except Exception as ex:
            print(strings["end_rss"])
            break
    elif(num == 2):
        sample_demo.cancel_asynchronous_listener()
        print(strings["stop_listener"])
    elif(num == 3):
        sample_demo.stop_send_speech_data()
        print(strings["cancel_send"])
    elif(num == 4):
        sample_demo.receive_ack_timeout()
    elif(num == 5):
        print(strings["completion_notice"])
        cmd = input().strip()
        sample_demo.set_completion_notice_necessity(cmd)
    elif(num == 99):
        print("completed demo.")
        #if(th_rss != None and th_rss.alive()):
        if(th_rss != None):
            # en:kill a thread to send a rss news data.
            # ja:RSSニュースデータ送信スレッド終了
            stop_event_rss.set()
            th_rss.join(5)
        sample_demo.cancel_asynchronous_listener()
        break 
    elif(num == -1):
        pass
    else:
        print(strings["irregular_num"])

    print(menu)


