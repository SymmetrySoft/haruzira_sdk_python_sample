# coding: utf-8

import haruzirasdk.hz_client_tcp_communication as hzcomm

class HzSampleDemo(object):

    # <summary>
    # Construct
    # </summary>
    # <param name="callback1">call back function when received a completion notice that made a speech</param>
    # <param name="callback2">call back function when occurred a exception(except disconnect)</param>
    # <param name="callback3">call back function when occurred a disconnecting issue</param>
    # <param name="callback4">call back function when received a 'Send Speech Recognition Command' message</param>
    def __init__(self, callback1 = None, callback2 = None, callback3 = None, callback4 = None):
        #en:create instance of ClientTcpCommunication class（regist call back functions and others）
        #ja:クライアント通信処理インスタンスの生成（コールバック関数の登録）
        self.__clt_tcp_comm = hzcomm.ClientTcpCommunication()
        self.__clt_tcp_comm.evNotifyCompleteSpeech = callback1
        self.__clt_tcp_comm.evNotifyMessageEvent = callback2
        self.__clt_tcp_comm.evNotifyReceivedDisconnectEvent = callback3
        self.__clt_tcp_comm.evNotifySendSpeechRecognitionCommand = callback4
        self.__server_port = 46000         #port number for access point
        self.__server_ip = "192.168.1.2"     #ip address for access point
        self.__receive_port = 46100        #port number for recieving asynchronous messages
        self.__clt_tcp_comm.ReceivedDataDecryptionKey = ""        #decryption key for that be encrypted voice command

    #region define accessor
    def get_server_port(self):
        return self.__server_port

    def set_server_port(self, value):
        self.__server_port = value

    def get_server_ip(self):
        return self.__server_ip

    def set_server_ip(self, value):
        self.__server_ip = value

    def get_receive_port(self):
        return self.__receive_port

    def set_receive_port(self, value):
        self.__clt_tcp_comm.ReceivePort = value
        self.__receive_port = value

    def get_rcv_data_decryption_key(self):
        return self.__clt_tcp_comm.ReceivedDataDecryptionKey

    def set_rcv_data_decryption_key(self, value):
        self.__clt_tcp_comm.ReceivedDataDecryptionKey = value
    #endregion

    #region define property
    server_port = property(get_server_port, set_server_port)
    server_ip = property(get_server_ip, set_server_ip)
    receive_port = property(get_receive_port, set_receive_port)
    rcv_data_decryption_key = property(get_rcv_data_decryption_key, set_rcv_data_decryption_key)
    #endregion


#region sending messsage
    # <summary>
    # en:send a speech data
    # ja:読み上げデータ送信
    # </summary>
    # <param name="sendText">text data to speech</param>
    # <param name="account">account name</param>
    # <param name="passwd">password</param>
    # <param name="encKey">encryption key</param>
    def send_speech_data(self, sendText, account = None, passwd = None, encKey = None):

        try:
            #en:make a set if you need that output of trace messages.(true:output, false:don't output)
            #ja:トレース出力設定(true:出力, false:出力しない)
            self.__clt_tcp_comm.setTraceOutPut(False)
            #en:you must make a set some values for access point, and do it if you need receiving asynchronous messages.
            #ja:接続先及び非同期メッセージ受信情報設定
            self.__clt_tcp_comm.ServerPortNo = self.server_port      #port number for access point
            self.__clt_tcp_comm.ServerIP = self.server_ip            #ip address for access point
            self.__clt_tcp_comm.ReceivePort = self.receive_port      #port number for recieving asynchronous messages

            print("Connecting")

            #en:make a set of speech options.
            #ja:読み上げオプションの設定
            self.__clt_tcp_comm.ReqSendDataAccountName = account
            self.__clt_tcp_comm.ReqSendDataPasswd = passwd  
            self.__clt_tcp_comm.ReqSendDataEncryptKey = encKey
            self.__clt_tcp_comm.ReqSendDataSpeechMode = hzcomm.HzSpeechTextMode.Text.value
            self.__clt_tcp_comm.ReqSendDataSpeechLevel = hzcomm.HzSpeechLevel.Normal.value
            #self.__clt_tcp_comm.ReqSendDataSpeechLocaleId = 0x0411
            self.__clt_tcp_comm.ReqSendDataSpeechLocaleId = 0x00
            self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Neutral.value
            self.__clt_tcp_comm.ReqSendDataSpeechAge = 0
            self.__clt_tcp_comm.ReqSendDataSpeechRepeat = 0
            self.__clt_tcp_comm.ReqSendDataText = sendText

            #en:send a speech data.
            #ja:読み上げデータ送信
            time_stamp = self.__clt_tcp_comm.sendSpeechDataEx()

            if (time_stamp is None):
                print("error code：[0x{02x}]".format(self.__clt_tcp_comm.ReceiveStatus))

            #en:the send result informations output
            #ja:送信時情報表示
            print("time stamp[{}]".format(time_stamp))
            print("sent data size：[{}]".format(self.__clt_tcp_comm.SendDataLength))
            print("SDK Version:[{}]".format(self.__clt_tcp_comm.Version))


        except Exception as ex:
            print(ex.args)
        finally:
            print("Disconnected")

    # <summary>
    # en:start a receiving thread for messages of asynchronous.
    # ja:非同期メッセージ受信スレッド(Listener)の起動
    # </summary>
    def start_asynchronous_listener(self):
        self.__clt_tcp_comm.setTraceOutPut(False)
        self.__clt_tcp_comm.startAsynchronousListener()

    # <summary>
    # en:cancel a receiving thread for messages of asynchronous.
    # ja:非同期メッセージ受信スレッドの終了
    # </summary>
    def cancel_asynchronous_listener(self):
        self.__clt_tcp_comm.setTraceOutPut(True)
        self.__clt_tcp_comm.cancelAsynchronousListener()

    # <summary>
    # en:cancel sending a speech data.
    # ja:読み上げデータ送信の停止
    # </summary>
    def stop_send_speech_data(self):
        self.__clt_tcp_comm.setTraceOutPut(True)
        self.__clt_tcp_comm.stopSendSpeechData()

    # <summary>
    # en:set and get, time out value to receive messasges of responce.
    # ja:応答受信時タイムアウト取得・設定
    # </summary>
    def receive_ack_timeout(self):
        #get value.
        print(str(self.__clt_tcp_comm.ReceiveAckTimeOut))
        #set value of 55 seconds. 
        self.__clt_tcp_comm.ReceiveAckTimeOut = 55
        #get value again.
        print(str(self.__clt_tcp_comm.ReceiveAckTimeOut.to_s))

    # <summary>
    # en:set SpeechCompletionNotice Necessity.
    # ja:読み上げ完了通知の要否設定
    # </summary>
    def set_completion_notice_necessity(self, val):
        if(val == "y"):
            self.__clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = hzcomm.HzSpCompletionNoticeNecessity.Need.value
        else:
            self.__clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = hzcomm.HzSpCompletionNoticeNecessity.NoNeed.value

#endregion
