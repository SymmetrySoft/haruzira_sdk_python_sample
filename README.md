# Haruzira SDK sample for Python #

Haruzira is a UWP APP.

## Requirements ##
* feedparser package.(pip install feedparser)


## Reference ##
Haruzira has been published in Microsoft Store. https://www.microsoft.com/store/apps/9nblggh516j3

It is localized in Japanese and English.

And the SDK User's Manual has been published on the Web site. https://haruzirasdke.wpblog.jp/


---------------------------------
## Example programs ##
* 1) send_text_demo.py, hz_sample_demo.py;  
    this program has following features.  
    - sends text to haruzira.  
    - receives command from haruzira.  
    - sends rss news contents.  
    - try the sdk options.  

### ###
* 2) simple_demo.py;  
    this program has a simple feature that it sends text message to haruzira.  
    you will be able to learn the simple message sending.

### ###
* 3) simple_command_receiver.py;  
    this program has a simple feature that it is received  sending command from haruzira.  
    you will be able to learn  the simple command receiving.  

### ###
* 4) voice_clock.py;  
    this program sends current time to haruzira; and haruzira will playback it by voice synthesize.  
    and haruzira can control it by using voice recognition from remote.  
    * demo movie  
    japanese site: https://symmetry-soft.com/html/tips/tts/others/tips_tts_voiceclock.htm#dotnet_exec  
