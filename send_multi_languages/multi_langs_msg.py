# coding: utf-8
from haruzirasdk import hz_client_tcp_communication as hzcomm

class MultiLangsMsg(object):

    # <summary>
    # Construct
    # </summary>
    # <param name="msg">a messsage text</param>
    # <param name="lang">locale ID(language code)</param>
    # <param name="gender">gender(HzSpeechGender)</param>
    def __init__(self, msg = None, locale = None, gender = None):
        self.__msg = ""
        self.__locale = 0x0409
        self.__gender = hzcomm.HzSpeechGender.Female
        if(msg is not None):
            self.__msg = msg
        if(locale is not None):
            self.__locale = locale
        if(gender is not None):
            self.__gender = gender

    #region define accessor
    def get_message(self):
        return self.__msg

    def set_message(self, value):
        self.__msg = value

    def get_locale(self):
        return self.__locale

    def set_locale(self, value):
        self.__locale = value

    def get_gender(self):
        return self.__gender

    def set_gender(self, value):
        self.__gender = value
    #endregion
