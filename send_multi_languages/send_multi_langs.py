# coding: utf-8
import time
import threading
from haruzirasdk import hz_client_tcp_communication as hzcomm
import multi_langs_msg as langs

#variable for an event.
comp_evt = threading.Event()

#region Define for callback functions.
# <summary>
# An event for the message of 'Speech Completion Notice'.
# </summary>
# <param name="result">submit result</param>
# <param name="time_stamp">time stamp</param>
def rcv_notify_complete_speech(result, time_stamp):
    #print("{0:s}：result[0x{1:02x}], time stamp[{2:s}]".format(strings["rec_comp"], result, time_stamp))
    print("recieved notify complete speech. time stamp[{0:s}]".format(time_stamp))
    comp_evt.set()
#endregion 

#A list for creating instances of the MultiLangsMsg class.
msgs = []
#create instance and initialize.
#Japanese.
msgs.append(langs.MultiLangsMsg("この電車は、まもなく新宿駅に停車します。", 0x0411, hzcomm.HzSpeechGender.Female))
#English(u.s).
msgs.append(langs.MultiLangsMsg("This train will soon stop at Shinjuku Station.", 0x0409, hzcomm.HzSpeechGender.Male))
#French.
msgs.append(langs.MultiLangsMsg("Ce train s'arrêtera bientôt à la gare de Shinjuku.", 0x040c, hzcomm.HzSpeechGender.Female))

#Initialize haruzira sdk.
clt_tcp_comm = hzcomm.ClientTcpCommunication()
clt_tcp_comm.ServerPortNo = 46000 #port number for access point
clt_tcp_comm.ServerIP = "192.168.1.7" #ip address for access point
clt_tcp_comm.evNotifyCompleteSpeech = rcv_notify_complete_speech

try:
    for msg in msgs:
        comp_evt.clear()
        clt_tcp_comm.ReqSendDataText = msg.get_message()
        clt_tcp_comm.ReqSendDataSpeechLocaleId = msg.get_locale()
        clt_tcp_comm.ReqSendDataSpeechGender = msg.get_gender().value
        clt_tcp_comm.ReqSendDataSpeechRepeat = 1
        timestamp = clt_tcp_comm.sendSpeechDataEx()
        if(timestamp is not None):
            print("time stamp[{0:s}]".format(timestamp))
        else:
            print("send error")
        #wait for receiving 'Speech Completion Notice' message.
        comp_evt.wait(20)
except Exception as ex:
    pass
finally:
    #close receiving listener.
    clt_tcp_comm.cancelAsynchronousListener()

